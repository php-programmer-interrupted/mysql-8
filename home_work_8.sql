-- Домашнее задание №8
USE shop;

-- С помощью команды UPDATE заполнить alias_name для всех категорий
UPDATE category SET alias_name='Women\'s clothing' WHERE id=1;
UPDATE category SET alias_name='Men\'s clothing' WHERE id=2;
UPDATE category SET alias_name='Women\'s shoes' WHERE id=3;
UPDATE category SET alias_name='Men\'s shoes' WHERE id=4;
UPDATE category SET alias_name='Hats' WHERE id=5;

-- Добавить новый бренд "Тетя Клава Company"
INSERT INTO brand (name) VALUES('Тетя Клава Company');

-- Удалить бренд "Тетя Клава Company"
DELETE FROM brand WHERE id=19;